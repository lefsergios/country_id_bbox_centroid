import psycopg2
import argparse
import sys



parser = argparse.ArgumentParser(description='Data for countries')
requiredArgs = parser.add_argument_group('ID , Envelope, Centroid')
requiredArgs.add_argument("-cnt", "--country", type=str, help="country ISO code", required=True)
args = parser.parse_args()

dbname = 'infinity'
user = 'lefteris'
host = 'infinity-pg-dev-2018-05-29.cw0nipflfk4w.us-east-1.rds.amazonaws.com'
password = '6fyVAvhTb6AfMPqx'


def connect_db(dbname, user, host, password):
    # Connect to Infinity Database
    try:
        conn = psycopg2.connect("dbname={} user={} host={} password={}".format(dbname, user, host, password))
        curs = conn.cursor()
        return curs, conn
    except Exception as e:
        raise EnvironmentError("Error connecting to Infinity database: {}".format(e))
        # the conn gets closed outside this function


try:
    curs, conn = connect_db(dbname, user, host, password)
    print("Connected to database")

    curs.execute(
        '''
        SELECT id, ST_AsEWKT(ST_Envelope(shape)),
        ST_ymin(shape), ST_ymax(shape), ST_xmin(shape), ST_xmax(shape),
        ST_AsEWKT(ST_Centroid(shape)) FROM boundaries
        WHERE id LIKE '{}%' AND level=0 AND deprecated=False
        '''.format(args.country)
    )

    db_result = [i for i in curs.fetchall()]

    print("id: " + db_result[0][0])
    print("bounding box: " + db_result[0][1][10:])
    print("latmin: {} , latmax: {} , lngmin: {} , lngmax: {}".format(db_result[0][2], db_result[0][3], db_result[0][4],
                                                                     db_result[0][5]))
    print("centroid: " + db_result[0][6][10:])

except Exception as e:
    print("Database error: {}".format(e))
    sys.exit(1)
finally:
    conn.close()
    print("Disconnected")